package vn.aptech.dao;

import vn.aptech.entities.Category;

public interface CategoryDAO extends DAO<Category, Integer> {
}
