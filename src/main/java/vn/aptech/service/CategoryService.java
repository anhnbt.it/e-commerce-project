/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.aptech.service;

import vn.aptech.entities.Category;

/**
 *
 * @author anhnbt
 */
public interface CategoryService extends BaseService<Category, Integer> {
}
