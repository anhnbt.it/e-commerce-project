USE quanlysanpham;
GO

--
-- Table structure for table `tbl_admin`
--
CREATE TABLE tbl_admin (
  id int IDENTITY(1,1) PRIMARY KEY,
  username varchar(30) DEFAULT NULL,
  email varchar(45) DEFAULT NULL,
  password varchar(45) DEFAULT NULL,
  created_at date NULL DEFAULT GETDATE(),
  updated_at datetime NULL DEFAULT CURRENT_TIMESTAMP
);

--
-- Table structure for table `brands`
--

CREATE TABLE tbl_brands (
  id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
  name varchar(45) NOT NULL,
);

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE tbl_categories (
  id tinyint NOT NULL IDENTITY(1,1) PRIMARY KEY,
  name varchar(45) NOT NULL,
);

--
-- Table structure for table `tbl_customers`
--

CREATE TABLE tbl_customers (
  id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
  name varchar(45) NOT NULL,
  email varchar(45) NOT NULL,
  password varchar(45) NOT NULL,
  phone varchar(45) DEFAULT NULL,
  date_of_birth date DEFAULT NULL,
  address varchar(45) DEFAULT NULL,
  city varchar(45) DEFAULT NULL,
  created_at datetime NULL DEFAULT CURRENT_TIMESTAMP,
);

--
-- Table structure for table `tbl_orders`
--
CREATE TABLE tbl_orders (
  id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
  customer_id int NOT NULL FOREIGN KEY (customer_id) REFERENCES tbl_customers (id),
  amount decimal(6,2) NOT NULL,
  status varchar(15) DEFAULT 'pending',
  created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
);

--
-- Table structure for table `tbl_suppliers`
--
CREATE TABLE tbl_suppliers (
  id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
  name varchar(45) NOT NULL
);

--
-- Table structure for table `tbl_options`
--
CREATE TABLE tbl_options (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	name varchar(45) NOT NULL,
	value varchar(45) NOT NULL
);

--
-- Table structure for table `tbl_products`
--
CREATE TABLE tbl_products (
  id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
  category_id tinyint NOT NULL FOREIGN KEY (category_id) REFERENCES tbl_categories (id),
  supplier_id int NOT NULL FOREIGN KEY (supplier_id) REFERENCES tbl_suppliers (id),
  brand_id int NOT NULL FOREIGN KEY (brand_id) REFERENCES tbl_brands (id),
  name varchar(45) NOT NULL,
  price decimal(10,2) NOT NULL,
  thumbnail_url varchar(255) DEFAULT NULL,
  description nvarchar,
  quantity int NOT NULL,
  status varchar(15) NOT NULL DEFAULT 'available',
  discount tinyint NOT NULL DEFAULT '0',
  view_count int NOT NULL DEFAULT '0',
  updated_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
);



--
-- Table structure for table `reviews`
--

CREATE TABLE tbl_reviews (
  id bigint NOT NULL IDENTITY(1,1) PRIMARY KEY,
  product_id int NOT NULL FOREIGN KEY (product_id) REFERENCES tbl_products (id),
  customer_id int NOT NULL FOREIGN KEY (customer_id) REFERENCES tbl_customers (id),
  rating tinyint NOT NULL DEFAULT '0',
  review_body varchar(255) DEFAULT NULL,
  status varchar(15) NOT NULL DEFAULT 'deactivated',
  created_at date NULL DEFAULT GETDATE(),
  updated_at datetime NULL DEFAULT CURRENT_TIMESTAMP,
);

--
-- Table structure for table `tbl_attributes`
--
CREATE TABLE tbl_attributes (
  id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
  product_id int FOREIGN KEY (product_id) REFERENCES tbl_products (id),
  option_id int FOREIGN KEY (option_Id) REFERENCES tbl_options (id),
  name varchar(45) DEFAULT NULL,
  value varchar(45) DEFAULT NULL
);



--
-- Table structure for table `images`
--

CREATE TABLE tbl_images (
  id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
  product_id int NOT NULL FOREIGN KEY (product_id) REFERENCES tbl_products (id),
  thumbnail_url varchar(255) DEFAULT NULL,
);

--
-- Table structure for table `orderdetails`
--
CREATE TABLE tbl_orderdetails (
  order_id int NOT NULL FOREIGN KEY (order_id) REFERENCES tbl_orders (id),
  product_id int NOT NULL FOREIGN KEY (product_id) REFERENCES tbl_products (id),
  quantity int NOT NULL DEFAULT '1',
  price decimal(10,2) NOT NULL,
  CONSTRAINT pk_orderdetails PRIMARY KEY (order_id,product_id)
);